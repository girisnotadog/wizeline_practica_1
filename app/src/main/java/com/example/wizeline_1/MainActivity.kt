package curso.wizeline2023.wizeline_1

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.wizeline_1.MainActivityViewModel
import curso.wizeline2023.wizeline_1.ui.theme.Wizeline_1Theme

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MainActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Wizeline_1Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Column() {
                        Greeting("Android")
                        Text("Remote config: ${viewModel.getNumberOfJokes()}")
                        Column() {
                            viewModel.jokesMutable.collectAsState(initial = listOf()).value.forEach {
                                Text("JOKE: $it")
                            }
                        }
                    }
                    
                }
            }
        }

        viewModel.getJokes()
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    Wizeline_1Theme {
        Greeting("Android")
    }
}